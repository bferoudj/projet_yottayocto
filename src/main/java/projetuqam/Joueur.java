package projetuqam;

import java.util.Scanner;
import java.io.Serializable;

public class Joueur implements Serializable {
    String typeJoueur = "";
    String pseudo = "";
    int numeroJoueur = 1;
    Langue langue;
    Joueur(String typeJoueur, String pseudo, int numeroJoueur, Langue langue){
        this.typeJoueur =typeJoueur;
        this.pseudo = pseudo;
        this.numeroJoueur = numeroJoueur;
        this.langue  = langue;
    }

    public String joueMain(){
        Scanner settingPartie = new Scanner(System.in);
        String main = "";
        if (typeJoueur=="SYSTEM") {
            main = MainDeJeux.mainAleatoire();
        } else {
            System.out.print("\033[H\033[2J");
            System.out.println("1\t " + pseudo + Langue.recupereSelonLangue("ChoixMain", this.langue.toString()));
            main = settingPartie.next();
        }
        return main;
    }

}
