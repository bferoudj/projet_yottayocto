package projetuqam;

import java.io.Serializable;
import java.util.Random;

public class MainDeJeux implements Serializable {
    private MainDeJeux() {throw new IllegalStateException("Utility class");}
    private static Random r = new Random();

    public static String mainAleatoire() {
        int min =0;
        int max =5;
        int res = 0;


        res = r.nextInt((max - min) + 1) + min;
        if (res==0){return "C";}
        if (res==1){return "F";}
        if (res==2){return "R";}
        if (res==3){return "L";}
        if (res==4){return "S";}
        return "R";
    }
}


