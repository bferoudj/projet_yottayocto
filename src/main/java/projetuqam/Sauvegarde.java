package projetuqam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Sauvegarde {

    private Sauvegarde() {throw new IllegalStateException("Utility class");}

    public static void sauvegarder(Joueur joueur1,Joueur joueur2, Jeux jeu) {
        try {
            FileOutputStream f = new FileOutputStream(new File("/tmp/myGame.txt"));
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            o.writeObject(joueur1);
            o.writeObject(joueur2);
            o.writeObject(jeu);
            o.close();
            f.close();



        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream"+e.toString());
        }
    }

    public static Jeux charger() {
        Jeux jeu1 = null;
        try {
            FileInputStream fi = new FileInputStream(new File("/tmp/myGame.txt"));
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Read objects
            Joueur pr1 = (Joueur) oi.readObject();
            Joueur pr2 = (Joueur) oi.readObject();
             jeu1 = (Jeux) oi.readObject();

            oi.close();
            fi.close();


        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jeu1;
    }
}
