package projetuqam;
import java.io.Serializable;


public class Langue implements Serializable {
    private String langue = "FR";
    Langue(String langue){
        this.langue = langue;
    }

    public String toString(){
        return this.langue;
    }
    static String recupereSelonLangue(String a, String b){
        String valeurConcat ;
        valeurConcat = a.concat("-").concat(b);

        switch(valeurConcat) {
            case "ResulatJoueurGagnant-FR" :
                return " Resultat du joueur gangant :";
            case "ResulatJoueurGagnant-EN" :
                return " results of the winner :";
            case "ResulatJoueurGagnant-ES" :
                return " resultado del jugador ganador :";
            case "ChoixMain-FR" :
                return " choisissez une main (C,R,F,S,L) ?";
            case "ChoixMain-EN" :
                return " choose a hand (C,R,F,S,L) ?";
            case "ChoixMain-ES" :
                return " elige una mano (C,R,F,S,L) ?";


            case "ErrorCombinaison-FR" :
                return " Erreur: Combinaison de jeux non valide! Combinaison valide F-R-C-L-S.";
            case "ErrorCombinaison-EN" :
                return " Error: Combinaison of game not valid! valid combinaison is F-R-C-L-S.";
            case "ErrorCombinaison-ES" :
                return " Error: Combinación de juego no válido! combinaison válido es F-R-C-L-S.";


            case "Nouvelle_partie-FR" :
                return "Nouvelle partie";
            case "Nouvelle_partie-EN" :
                return "New game";
            case "Nouvelle_partie-ES" :
                return "Nuevo juego";
            case "Merci_Bientot-FR" :
                return "Merci d'avoir jouer! à bientôt";
            case "Merci_Bientot-EN" :
                return "Thank you!";
            case "Merci_Bientot-ES" :
                return "gracias!";
            case "ScoreJoueur-FR" :
                return "Score du joueur ";
            case "ScoreJoueur-EN" :
                return "score of the player ";
            case "ScoreJoueur-ES" :
                return "puntuación del jugador ";
            case "NbrPartie-FR" :
                return "Nombre de parties jouées =";
            case "NbrPartie-EN" :
                return "Number of game played =";
            case "NbrPartie-ES" :
                return "Número de juego jugado =";
            case "ChoixNonValide-FR" :
                return "Choix non valide!!!";
            case "ChoixNonValide-EN" :
                return "Non valid choice!!!";
            case "ChoixNonValide-ES" :
                return "Elección no válida!!!";
            case "LangueChoix-FR" :
                return "Quel est votre choix de langue(FR,EN,ES)?";
            case "LangueChoix-EN" :
                return "What is your language choice (FR,EN,ES)?";
            case "LangueChoix-ES" :
                return "Cuál es tu elección de idioma (FR,EN,ES)?";
            case "NombreJoueur-FR" :
                return "Quel est le nombre de joueurs humain (1,2)?";
            case "NombreJoueur-EN" :
                return "How many human players (1,2)?";
            case "NombreJoueur-ES" :
                return "Cuantos jugadores humanos?";
            case "Nom_Joueur1-FR" :
                return "Quel est le nom du 1er joueur ?";
            case "Nom_Joueur1-EN" :
                return "What is the name of the 1st player?";
            case "Nom_Joueur1-ES" :
                return "Cuál es el nombre del 1er jugador?";
            case "Nom_Joueur2-FR" :
                return "Quel est le nom du 2eme joueur ?";
            case "Nom_Joueur2-EN" :
                return "What is the name of the 2nd player?";
            case "Nom_Joueur2-ES" :
                return "Cual es el nombre del segundo jugador?";
            case "Score-FR" :
                return "Score";
            case "Score-EN" :
                return "Score";
            case "Score-ES" :
                return "Puntuación";
            case "Langue-FR" :
                return "Langue";
            case "Langue-EN" :
                return "Language";
            case "Langue-ES" :
                return "Idioma";
            case "Quitter-FR" :
                return "Quitter";
            case "Quitter-EN" :
                return "End Game";
            case "Quitter-ES" :
                return "Fin del juego";
            case "ChoixSVP-FR" :
                return "Veuillez entrer votre choix:";
            case "ChoixSVP-EN" :
                return "Please enter your choice:";
            case "ChoixSVP-ES" :
                return "Por favor ingrese su elección:";
            case "Sauvegarder-FR" :
                return "Sauvegarde";
            case "Sauvegarder-EN" :
                return "Save";
            case "Sauvegarder-ES" :
                return "Salvar:";
            case "Charger-FR" :
                return "Chargement";
            case "Charger-EN" :
                return "Load";
            case "Charger-ES" :
                return "Carga";
            case "Partie-FR" :
                return "Partie";
            case "Partie-EN" :
                return "Game";
            case "Partie-ES" :
                return "Juego";
            case "Gagnant-FR" :
                return "Gagnant";
            case "Gagnant-EN" :
                return "Winner";
            case "Gagnant-ES" :
                return "Ganador";
            case "ResultatPartie-FR" :
                return "Resultats des parties gagnantes";
            case "ResultatPartie-EN" :
                return "Winning Game results";
            case "ResultatPartie-ES" :
                return "Resultados de primero juegos";
            default :
                throw new IllegalArgumentException("Erreur: Combinaison de clé -langue non valide!"+a+"-"+b);
        }    }
}
