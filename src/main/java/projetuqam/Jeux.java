package projetuqam;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import java.io.Serializable;


public class Jeux implements Serializable {
    static String humain  = "HUMAN";
    static String system  = "SYSTEM";
    private Langue langue = new Langue("FR");
    Jeux(){

    }
    ArrayList parties= new ArrayList();
    Joueur joueur1;
    Joueur joueur2;
    Regle regle;
    Jeux(Joueur joueur1, Joueur joueur2, Regle regle, Langue langue){
        this.parties = new ArrayList();
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
        this.regle = regle;
        this.langue = langue;
    }


    public static void main(String [] args){

        Scanner settingJeux = new Scanner(System.in);

        System.out.print("\033[H\033[2J");
        System.out.println("1\t Quel est votre choix de langue/What is your language choice (FR,EN,ES)?");
        Langue langueChoisi = new Langue(settingJeux.next());
        // menu du jeux
        System.out.print("\033[H\033[2J");
        System.out.println("1\t "+Langue.recupereSelonLangue("NombreJoueur",langueChoisi.toString()));
        int nbrJoueur = settingJeux.nextInt();
        Joueur joueur1;
        Joueur joueur2;
        Jeux je;
        if (nbrJoueur==1) {

            System.out.println("1\t "+Langue.recupereSelonLangue("Nom_Joueur1",langueChoisi.toString()));

            joueur1 = new Joueur(humain,settingJeux.next(),1,langueChoisi);
            joueur2 = new Joueur(system,"SYSTEM",2,langueChoisi);
            je = new Jeux(joueur1,joueur2,new Regle(langueChoisi),langueChoisi);
            je.debute();

        } else if (nbrJoueur==2) {

            System.out.println("1\t "+Langue.recupereSelonLangue("Nom_Joueur1",langueChoisi.toString()));

            joueur1 = new Joueur("HUMAIN",settingJeux.next(),1,langueChoisi);

            System.out.println("1\t "+Langue.recupereSelonLangue("Nom_Joueur2",langueChoisi.toString()));

            joueur2 = new Joueur("HUMAIN",settingJeux.next(),2,langueChoisi);
            je = new Jeux(joueur1,joueur2,new Regle(langueChoisi),langueChoisi);
            je.debute();
        }









    }//end of the main method

    public void debute(){
        boolean continueJeux = true;
        while (continueJeux) {
            Scanner in = new Scanner(System.in);
            // menu du jeux

            System.out.println("1\t "+Langue.recupereSelonLangue("Nouvelle_partie",this.langue.toString()));
            System.out.println("2\t "+Langue.recupereSelonLangue("Score",this.langue.toString()));
            System.out.println("3\t "+Langue.recupereSelonLangue("Langue",this.langue.toString()));
            System.out.println("4\t "+Langue.recupereSelonLangue("Sauvegarder",this.langue.toString()));
            System.out.println("5\t "+Langue.recupereSelonLangue("Charger",this.langue.toString()));
            System.out.println("6\t "+Langue.recupereSelonLangue("Quitter",this.langue.toString()));
            System.out.println(Langue.recupereSelonLangue("ChoixSVP",this.langue.toString()));

            //choix
            int choice = in.nextInt();

            //selon choix voulu
            switch (choice) {
                case 1:
                    System.out.print("\033[H\033[2J");
                    System.out.println(Langue.recupereSelonLangue("Nouvelle_partie",this.langue.toString()));
                    Partie partie = new Partie(this.parties.size(),this.joueur1,this.joueur2,this.regle, this.langue);
                    partie.jouerPartie();
                    this.parties.add(partie);
                    break;
                case 2:
                    System.out.println(Langue.recupereSelonLangue("ScoreJoueur", this.langue.toString())+this.joueur1.pseudo+" = "+this.getScoreJoueur(joueur1));

                    System.out.println(Langue.recupereSelonLangue("ScoreJoueur", this.langue.toString())+this.joueur2.pseudo+" = "+this.getScoreJoueur(joueur2));
                    System.out.println("-------------"+Langue.recupereSelonLangue("ResultatPartie", this.langue.toString())+"---------------");
                    for (int i=0;i<parties.size();i++) {
                        if (((Partie) parties.get(i)).joueurGagnant != null) {
                            System.out.println(Langue.recupereSelonLangue("" + "Partie", this.langue.toString()) + ":" + i + " " + Langue.recupereSelonLangue("Gagnant", this.langue.toString()) + " :" + ((Partie) parties.get(i)).joueurGagnant.pseudo);

                        }
                    }

                    break;
                case 6:
                    System.out.println(Langue.recupereSelonLangue("Merci_Bientot",this.langue.toString()));
                    continueJeux = false;
                    break;
                case 3:
                    System.out.print("\033[H\033[2J");
                    System.out.println("1\t "+Langue.recupereSelonLangue("LangueChoix", this.langue.toString()));
                    this.langue = new Langue(in.next());
                    continueJeux = true;
                    break;
                case 4:
                    Sauvegarde.sauvegarder(this.joueur1,this.joueur2,this);
                    System.out.println("save");
                    continueJeux = true;
                    break;
                case 5:
                    Jeux jeuTmp = Sauvegarde.charger();
                    this.joueur1 = jeuTmp.joueur1;
                    this.joueur2 = jeuTmp.joueur2;
                    this.parties.clear();
                    this.parties.addAll(jeuTmp.parties);
                    continueJeux = true;
                    break;
                default:
                    System.out.println(Langue.recupereSelonLangue("ChoixNonValide", this.langue.toString()));
            }//end of switch
        }

    }

    public void afficheScoreJoueurs(){
        System.out.println(Langue.recupereSelonLangue("ScoreJoueur", this.langue.toString())+this.joueur1.pseudo+" = "+this.getScoreJoueur(joueur1));
        System.out.println(Langue.recupereSelonLangue("ScoreJoueur", this.langue.toString())+this.joueur2.pseudo+" = "+this.getScoreJoueur(joueur2));
    }

    public void afficheNbrPartiesJouer(){
        System.out.println(Langue.recupereSelonLangue("NbrPartie", this.langue.toString())+this.getNbrPartieJouer());
    }

    public int getScoreJoueur(Joueur joueur){
        Iterator it = parties.iterator();
        int score = 0;
        while(it.hasNext()) {
            Partie partie = (Partie)it.next();
            if ((partie.joueurGagnant!=null)&&(partie.joueurGagnant.numeroJoueur==joueur.numeroJoueur)){
                    score++;
                }
        }
        return score;
    }

    public int getNbrPartieJouer(){

        return this.parties.size();
    }
}
