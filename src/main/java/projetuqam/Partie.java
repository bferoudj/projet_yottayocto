package projetuqam;

import java.io.Serializable;

public class Partie implements Serializable {
    int index = 0;
    Joueur joueur1;
    Joueur joueur2;
    Joueur joueurGagnant=null;
    String main1 = "";
    String main2 = "";
    Regle regle;
    Langue langue;
    public boolean partieEnCours = true;

    public Partie(int index,Joueur joueur1,Joueur joueur2, Regle regle, Langue langue){
        this.index =index;
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
        this.regle = regle;
        this.langue = langue;
    }

    public void jouerPartie(){

        int res = this.regle.resultatJeux(this.joueur1.joueMain(),this.joueur2.joueMain());
        if (res==1){
            this.joueurGagnant = joueur1;
        }

        if (res==-1){
            this.joueurGagnant = joueur2;
        }
        if (this.joueurGagnant!=null) {
            System.out.println(Langue.recupereSelonLangue("ResulatJoueurGagnant", this.langue.toString()) + this.joueurGagnant.pseudo);
        }
        this.partieEnCours = false;
    }


    public void terminerPartie(Joueur joueurGagnant){
        this.joueurGagnant = joueurGagnant;
        this.partieEnCours = false;
    }

    public void terminerPartie(){
        this.partieEnCours = false;
    }
}
