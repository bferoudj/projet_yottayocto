package projetuqam;

import java.io.Serializable;
import java.util.logging.*;
public class Regle implements Serializable {
        int resultat = 0;
        Langue langue;
        Regle(Langue langue){
            this.langue = langue;
        }

        public boolean estJeuxEgale(String a, String b){

            return a.equals(b);
        }

        public boolean estJeuxGagnant(String a, String b){

            String valeurConcat ;

            valeurConcat = a.concat("-").concat(b);

            switch(valeurConcat) {
                case "R-C" :
                    return true;
                case "C-F" :
                    return true;
                case "F-R" :
                    return true;
                case "L-S" :
                    return true;
                case "L-F" :
                    return true;
                case "R-L" :
                    return true;
                case "S-C" :
                    return true;
                case "C-L" :
                    return true;
                case "F-S" :
                    return true;
                case "S-R" :
                    return true;
                case "C-R" :
                    return false;
                case "F-C" :
                    return false;
                case "R-F" :
                    return false;
                case "C-C" :
                    return false;
                case "F-F" :
                    return false;
                case "R-R" :
                    return false;
                case "S-L" :
                    return false;
                case "F-L" :
                    return false;
                case "L-R" :
                    return false;
                case "C-S" :
                    return false;
                case "L-C" :
                    return false;
                case "S-F" :
                    return false;
                case "R-S" :
                    return false;
                default :

                    throw new IllegalArgumentException(Langue.recupereSelonLangue("ErrorCombinaison", this.langue.toString()));
            }
        }

    public boolean estJeuxPerdant(String a, String b){

        String valeurConcat ;
        valeurConcat = a.concat("-").concat(b);
        return !this.estJeuxGagnant(a,b);
    }
    public int resultatJeux(String a, String b){
        String valeurConcat ;
        valeurConcat = a.concat("-").concat(b);


        switch(valeurConcat) {
            case "R-C" :
                return 1;
            case "C-F" :
                return 1;
            case "F-R" :
                return 1;
            case "L-S" :
                return 1;
            case "L-F" :
                return 1;
            case "R-L" :
                return 1;
            case "S-C" :
                return 1;
            case "C-L" :
                return 1;
            case "F-S" :
                return 1;
            case "S-R" :
                return 1;
            case "C-R" :
                return -1;
            case "F-C" :
                return -1;
            case "R-F" :
                return -1;
            case "C-C" :
                return 0;
            case "F-F" :
                return 0;
            case "R-R" :
                return 0;
            case "S-S" :
                return 0;
            case "L-L" :
                return 0;
            case "S-L" :
                return -1;
            case "F-L" :
                return -1;
            case "L-R" :
                return -1;
            case "C-S" :
                return -1;
            case "L-C" :
                return -1;
            case "S-F" :
                return -1;
            case "R-S" :
                return -1;
            default :
                throw new IllegalArgumentException(Langue.recupereSelonLangue("ErrorCombinaison", this.langue.toString()));
        }    }
}
